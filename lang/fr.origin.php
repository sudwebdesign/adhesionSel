<?php #0009.4.2.0
$LANG = array(
'L_LFORMAT' => 'fr-FR',
#adhesion admin.php hooks
'L_ARTS_VIEW_TITLE' => 'Visualiser les articles de cette personne',# cet adhérent
'L_ARTS_ADMIN_TITLE' => 'Administrer les articles de cette personne',# cet adhérent
'L_ARTS_SHIFT_TITLE' => 'Basculer et aller éditer les articles et commentaires de cette personne',# cet adhérent
'L_SPACE_SHIFT' => 'Compte utilisateur',
'L_IN_COURSE' => 'En cours',
'L_SHIFT' => 'Y Aller',
'L_NONE' => 'AUCUN',
#my-item
'L_ARTICLE_CATEGORIES' => 'Catégories S.E.L.',#sidebar
'L_CATEGORY_HOME_PAGE' => 'Accueil Inscrits',# adhérents S.E.L.
#my-items
'L_CATEGORY_HOME' => 'Accueil Inscrits',# adhérents S.E.L.
#sidebar
'L_CATEGORIES' => 'Catégories S.E.L.',#Thématiques
'L_TERM' => 'Terme clé S.E.L',#tag
'L_TERMS' => 'Termes clés S.E.L',#tags
'L_PLACE' => 'Espace S.E.L',#categorie
'L_ARCHS' => 'Archives S.E.L',#archives
'L_LATEST_COMMENTS' => 'Derniers commentaires S.E.L',
'L_SIDEBAR_MY_SEL_SPACE' => 'Mon SEL&nbsp;:',
'L_OF_THIS_ART' => ' pour cette personne',# cet adhérent# de cet articles idebar.php
'L_OF_THIS_ARTS' => ' pour cette personne',# cet adhérent# de cet articles idebar.php
'L_OF_THIS_TERM' => ' du terme clé',# bonux sidebar.php
'L_ART_OF' => 'Article S.E.L de ',
'L_ART' => 'Article S.E.L',
'L_ARTS' => 'Articles S.E.L.',#Articles S.E.L.
'L_ARTS_OF' => 'Articles S.E.L. de ',#Articles S.E.L.
'L_MY_ART_NEW' => 'Nouvel Article S.E.L.',
'L_ANNUARY_SEE_ARTS' => 'Voir les articles de ',
'L_CALL' => 'Appeler',
'L_CALL_TITLE' => 'Voir ses infos pour contacter par courriel ou téléphone ',#todo sms:num in annuary
'L_MY_MEDIAS' => 'Mes Médias',
'L_MY_ARTS' => 'Mes Articles',#Gérer
'L_MY_ART' => 'Mon Article',#✚
'L_MY_COMS' => 'Mes Commentaires',#Gérer
'L_MY_COM' => 'Mon Commentaire',
'L_MY_COM_NEW' => 'Nouveau Commentaire',
'L_SPHERE' => 'Accueil inscrits',# adhérents S.E.L.  Accueil S.E.L.Articles S.E.L.//Mon Espace SEL, tous les articles
'L_THEME_EDITOR' => 'Thème de l\'éditeur de texte',
);
