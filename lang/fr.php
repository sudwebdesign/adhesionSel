<?php #0009.5.0.0 : ’ is not '
$LANG = array(
'L_LFORMAT' => 'fr-FR',
#adhesion admin.php hooks
'L_NO_ARTS_VIEW_TITLE' => 'Mes Annonces jamais ouvert pour cette personne',# cet adhérent
'L_ARTS_VIEW_TITLE' => 'Visualiser les articles de cette personne',# cet adhérent
'L_ARTS_ADMIN_TITLE' => 'Administrer les articles de cette personne',# cet adhérent
'L_ARTS_SHIFT_TITLE' => 'Basculer et aller éditer les articles et commentaires de cette personne',# cet adhérent
'L_SPACE_SHIFT' => 'Compte utilisateur',
'L_IN_COURSE' => 'En cours',
'L_SHIFT' => 'Y Aller',
'L_NONE' => 'AUCUN',
#my-item
'L_ARTICLE_CATEGORIES' => 'Rubriques',# S.E.L.',#sidebar
'L_CATEGORY_HOME_PAGE' => 'Accueil inscrit',# S.E.L.',# Inscrits adhérents S.E.L.
#my-items
'L_CATEGORY_HOME' => 'Accueil inscrits',# S.E.L.',# Inscrits adhérents S.E.L.
#sidebar
'L_CATEGORIES' => 'Rubriques',# S.E.L.',#
'L_TERM' => 'Terme clé',# S.E.L.',#tag
'L_TERMS' => 'Termes clés',# S.E.L.',#tags
'L_PLACE' => 'Rubrique',# S.E.L.',#Espace Thématique categorie
'L_ARCHS' => 'Archives',# S.E.L.',#archives
'L_LATEST_COMMENTS' => 'Derniers commentaires',# S.E.L.',
'L_SIDEBAR_MY_SEL_SPACE' => 'Mon espace&nbsp;:',
'L_OF_THIS_ART' => ' pour cette personne',# cet adhérent# de cet articles sidebar.php
'L_OF_THIS_ARTS' => ' pour cette personne',# cet adhérent# de cet articles sidebar.php
'L_OF_THIS_TERM' => ' du terme clé',# bonux sidebar.php
'L_ART_OF' => 'Annonce de ',
'L_ART' => 'Annonce',# S.E.L.',
'L_ARTS' => 'Annonces',# S.E.L.',#Articles S.E.L.
'L_ARTS_OF' => 'Annonces de ',#Articles S.E.L.
'L_MY_ART_NEW' => 'Créer une Annonce',# S.E.L.',#Nouvel Article S.E.L.
'L_ANNUARY_SEE_ARTS' => 'Voir les annonces de ',
'L_CALL' => 'Appeler',
'L_CALL_TITLE' => 'Voir ses infos pour contacter par mél ou par téléphone ',#todo sms:num in annuary
'L_MY_MEDIAS' => 'Mes Médias',
'L_MY_ARTS' => 'Mes Annonces',#Gérer
'L_MY_ART' => 'Mon Annonce',#✚
'L_MY_COMS' => 'Mes Commentaires',#Gérer
'L_MY_COM' => 'Mon Commentaire',
'L_MY_COM_NEW' => 'Nouveau Commentaire',
'L_SPHERE' => 'Les Annonces',#ccueil inscrits',# inscrits adhérents S.E.L.  Accueil S.E.L.Articles S.E.L.//Mon Espace SEL, tous les articles
'L_SPHERE_HOME' => 'En Accueil',#',# inscrits adhérents S.E.L.  Accueil S.E.L.Articles S.E.L.//Mon Espace SEL, tous les articles
'L_THEME_EDITOR' => 'Thème de l\'éditeur de texte',
);