<?php # This file is part of SunEditor plugin for pluxml : @date 12/04/2019 : http://sudwebdesign.free.fr/
require('ajax_upload_init.php');
if(!isset($_SESSION['user'])) exit;

//www.tinymce.com/docs/advanced/php-upload-handler/ (adapted)
  /*******************************************************
   * Only these origins will be allowed to upload images *
   ******************************************************/

  $accepted_origins = array($plxMotor->racine);//,'http://localhost', 'http://192.168.1.1', 'http://my-domain.me'

  /*********************************************
   * Change this line to set the upload folder *
   *********************************************/
  $baseFolder = 'data'.DIRECTORY_SEPARATOR.'medias'.$pluginName.DIRECTORY_SEPARATOR.$_SESSION['adhesion'];
  $imageFolder = DIRECTORY_SEPARATOR.'blobSun';

//  $imageFolder = $plxMotor->plxPlugins->aPlugins[$pluginName]->getParam('datamedia').$_SESSION['adhesion'].DIRECTORY_SEPARATOR.'blobSun';
#data/medias/../mediasadhesionSel/

  reset ($_FILES);
  $temp = current($_FILES);
  if (is_uploaded_file($temp['tmp_name'])){
    if (isset($_SERVER['HTTP_ORIGIN'])) {
// same-origin requests won't set an origin. If the origin is set, it must be valid.
      if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
      } else {
        header("HTTP/1.0 403 Origin Denied");
        return;
      }
    }

/*
      If your script needs to receive cookies, set images_upload_credentials : true in
      the configuration and enable the following two headers.
*/
     header('Access-Control-Allow-Credentials: true');
     header('P3P: CP="There is no P3P policy."');

// Sanitize input
    if (preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])) {
        header("HTTP/1.0 500 Invalid file name.");
        return;
    }

// Verify extension
    if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "jpeg", "png"))) {
        header("HTTP/1.0 500 Invalid extension.");
        return;
    }

/*    $a = $r = '';*/
// normal src cimix suneditor

    $a = PLX_ROOT;#.$plxMotor->aConf['medias'];//$plxMotor->racine.$plxMotor->aConf['medias'];
    $r = $plxMotor->racine;#.$plxMotor->aConf['medias'];

//  Accept upload if there was no origin, or if it is an accepted origin
    //~ $absoImageFolder = PLX_ROOT.$plxMotor->plxPlugins->aPlugins[$pluginName]->getParam('folder').$imageFolder.DIRECTORY_SEPARATOR;//absolute
    //~ $relaImageFolder = PLX_TOOT.$plxMotor->plxPlugins->aPlugins[$pluginName]->getParam('folder').$imageFolder.DIRECTORY_SEPARATOR;//relative
    $absoImageFolder = $a.$baseFolder.$imageFolder.DIRECTORY_SEPARATOR;//absolute
    $relaImageFolder = $r.$baseFolder.$imageFolder.DIRECTORY_SEPARATOR;//relative

//var_dump(PLX_TOOT,PLX_CORE,PLX_ROOT,$plxMotor->plxPlugins->aPlugins[$pluginName]->getParam('datamedia'),$imageFolder,$absoImageFolder,$relaImageFolder);
//var_dump($a.$baseFolder,$absoImageFolder,$relaImageFolder);

    if(!is_dir($a.$baseFolder)) mkdir($a.$baseFolder);// if not, create user media folder
    if(!is_dir($absoImageFolder)) mkdir($absoImageFolder);// if not, create user media upload folder
    $iseo = '';
    if(isset($_SERVER['HTTP_REFERER'])){
     $esrap = parse_url($_SERVER['HTTP_REFERER']);//scheme host path query
     //~ $iseo = str_replace(array('/core/admin/','.php'),'',$esrap['path']).'_';//
     $iseo = explode(DIRECTORY_SEPARATOR,$esrap['path']);//
     $iseo = str_replace('.php','',end($iseo)).'_';//
     if(isset($esrap['query'])){
      $qr = explode('=',$esrap['query']);
      $pg = explode('.',$esrap['query']);#index.php  ?   my-art.html&a=0111
      if(!empty($pg[0]))
	$iseo = '';//remove index_
      $iseo .= $pg[0] . '_' . str_replace($qr[0].'=','',$esrap['query']).'_';
     }
    }
    $date = date('Y-m-d_H-m-i').'_';
    $size = $temp['size'];
    move_uploaded_file($temp['tmp_name'], $absoImageFolder . $iseo . $date . $temp['name']);
    //  Respond to the successful upload with JSON. (See https://ckeditor.com/docs/ckeditor4/latest/guide/dev_file_upload.html)
    header('Content-Type: text/javascript');#JSON 4 ALL PHP
//echo '/*console.log("ajax up to:","Relatve : '.$relaImageFolder.'","Absolute : '.$relaImageFolder.'");*/';
    //echo '{"uploaded": 1,"fileName": "'.$iseo . $date . $temp['name'].'","url": "'.$relaImageFolder . $iseo . $date . $temp['name'].'"}';
    //echo '{"result": {"name"= "'.$iseo . $date . $temp['name'].'","size"= "'.$size.'","url"= "'.$relaImageFolder . $iseo . $date . $temp['name'].'"}}';



    echo json_encode(array('result' => array( 0 => array('name'=>$iseo . $date . $temp['name'], 'size'=>$size, 'url'=>$relaImageFolder . $iseo . $date . $temp['name']))));
/*
                    const fileList = response.result;
                    for (let i = 0, len = (update && fileList.length > 0 ? 1 : fileList.length); i < len; i++) {
                        this.plugins.image.create_image.call(this, fileList[i].url, linkValue, linkNewWindow, width, align, update, updateElement, {name: fileList[i].name, size: fileList[i].size});
                    }
*/
  } else {
    // Notify editor that the upload failed
    header('Content-Type: text/javascript');#JSON 4 ALL PHP
    header("HTTP/1.0 500 Server Error");
//    echo '/*console.log("error ajax up to:","Relatve : '.$relaImageFolder.'","Absolute : '.$relaImageFolder.'");*/';
//    echo '{"result": {"name": "'.$iseo . $date . $temp['name'].'","url": "'.$relaImageFolder . $iseo . $date . $temp['name'].'","errorMessage": {"message": "500 Server Error"}}}';
    echo json_encode(array('result' => array( 0 => array('name'=>$iseo . $date . $temp['name'], 'url'=>$relaImageFolder . $iseo . $date . $temp['name'], 'errorMessage'=>'500 Server Error'))));
  }
