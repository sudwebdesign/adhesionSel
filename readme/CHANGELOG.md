# PluXml plugin (adhesionSel)[http://sudwebdesign.free.fr/index.php?article39]


## 00.00.00.0009.4.2.0 : 14/09/2019 : Rubriques & annonces ##
* To Rubriques & annonces : LANG FR : De Catégories à Rubriques & Articles à Annonces : fichier .rub.an supprimé, les classique art & cat SEL sont renommé en .origin.php
* Improve in Rub.An file  languages terms with S.E.L. & remove it
* Good breadcrumbs fixes
* ::: cat.html : Fix missing double quote "
* ::: items.html (sphere, arts, ...) : Fix oups + add link to first page
* ::: tags.html (term) : Fix accentuated words in url
* Fix bonux in sidebar in term mode
* +'L_OF_THIS_TERM' => ' du terme clé',# bonux sidebar.php
* Fix missing pagination in arts mode (One page)
* ::: plxMotorConstructLoadPluginsadhesion : fix lost $arts_ad on page/#
* ::: plxMotorDemarrageBegin : add case arts

## 00.00.00.0009.4.1.0 : 13/09/2019 ##
* add link in breadcrumbs for page > 1
* Fix htmls avec id=post-#idart not unique : cat.html, items.html, term.html : MAJ pour infiniteAjaxScroll show direct page link
* echo $plxShow->artId(); replaced by $this->plxMotor->plxRecord_arts->f($pluginName) #idea for all : or $id for tags
* <article class="article" id="post-<?php echo $this->plxMotor->plxRecord_arts->f($pluginName)/* or $id ;) $plxShow->artId()*/ ?>">

## 00.00.00.0009.3.3.3 - 27/08/2019 ##
*Première publié
