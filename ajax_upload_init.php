<?php
# ------------------ BEGIN LICENSE BLOCK ------------------
#
# This file is part of SunEditor plugin for pluxml : http://sudwebdesign.free.fr/
#
# Copyright © 2018-2019 Thomas Ingles
# Copyright © 2010-2011 Stephane Ferrari and contributors
# Copyright © 2008-2009 Florent MONTHEL and contributors
# Copyright © 2006-2008 Anthony GUERIN
# Licensed under the GPL license.
# See http://www.gnu.org/licenses/gpl.html
#
# ------------------- END LICENSE BLOCK -------------------
/* dérivé du plugin Cmixml, dérivé du plugin Gutuma et PluXml (gutuma/news/inc/_pluxml.php & from core/admin/prepend.php)
 * @version 2.14.0
 * @date	12/04/2019
 * @author	Thomas Ingles : sudwebdesign.free.fr
*/
$pluginName = basename(__DIR__);
# Définition des constantes de pluxml
define('PLX_TOOT', '..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);// TOOT 4 SYMBIOLINK
//PLX_ROOT détermine le chemin des params de XMLFILE_PARAMETERS * uncomment this 3 lines (below) if tinymce is symlinked in an other PluXml (I use it 4 my dev Th0m@s)
$gu_sub = explode('plugins',$_SERVER['DOCUMENT_ROOT'].$_SERVER['PHP_SELF']);
$gu_sub = str_replace($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR,'',$gu_sub[0]);//4 found subdir where plx is
define('PLX_ROOT',$_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.$gu_sub);// OR PLX_TOOT
#define('PLX_ROOT', PLX_TOOT);# Normal config, tiny is in plugins folder 4 real * comment this line if tiny is symlinked & in an other PluXml
define('PLX_CORE', PLX_ROOT.'core'.DIRECTORY_SEPARATOR);

/* test : unwork # Définition des constantes de pluxml
$toot = '..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR;
const PLX_TOOT = $toot;//define('PLX_TOOT', '..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);// TOOT 4 SYMBIOLINK
//PLX_ROOT détermine le chemin des params de XMLFILE_PARAMETERS * uncomment this 3 lines (below) if tinymce is symlinked in an other PluXml (I use it 4 my dev Th0m@s)
$gu_sub = explode('plugins',$_SERVER['DOCUMENT_ROOT'].$_SERVER['PHP_SELF']);
$gu_sub = str_replace($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR,'',$gu_sub[0]);//4 found subdir where plx is
$gu_sub = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.$gu_sub;
const PLX_ROOT = $gu_sub;//define('PLX_ROOT',$_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.$gu_sub);// OR PLX_TOOT
#const PLX_ROOT = PLX_TOOT; #define('PLX_ROOT', PLX_TOOT);# Normal config, Sun is in plugins folder 4 real * comment this line if tiny is symlinked & in an other PluXml
$core = PLX_ROOT.'core'.DIRECTORY_SEPARATOR;
const PLX_CORE = $core;//define('PLX_CORE', PLX_ROOT.'core'.DIRECTORY_SEPARATOR);
*/

$plx = (isset($_GET['plx'])&&!empty($_GET['plx']))?$_GET['plx']:'5.7';
if(version_compare($plx, '5.9.0', '<')){#legacy & next gen (6.0)
  include(PLX_ROOT.'config.php');
}

include(PLX_CORE.'lib'.DIRECTORY_SEPARATOR.'config.php');

# On verifie que PluXml est installé
if(!file_exists(path('XMLFILE_PARAMETERS'))) {
	header('Location: '.PLX_ROOT.'install.php');
	exit;
}
# On démarre la session
session_start();

# On inclut les librairies nécessaires
foreach(explode('·','date·glob·utils·msg·record·motor·admin·encrypt·medias·plugins·token·capcha·erreur·feed·show') as $glx_class)
 include_once(PLX_CORE.'lib/class.plx.'.$glx_class.'.php');

# Creation de l'objet principal et lancement du traitement
$plxMotor = plxMotor::getInstance();
if (!isset($plxMotor->plxPlugins->aPlugins[$pluginName])) exit;
//$plxMotor->mode='ckeditor_upload';//4 future ::: & solved bug header 404 in demarrage & prechauffage funct() but in reality is'nt util here (more perf ;-)
